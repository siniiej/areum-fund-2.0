var usdabig = document.getElementById("usda-chart-big");
var usdaBigChart = new Chart(usdabig, {
  type: 'line',
  data: {
    labels: ["5", "6", "7", "8", "9", "10"],
    datasets: [{
      label: 'USDA price',
      data: [12, 19, 3, 5, 2, 3],
      borderColor: [
        'rgba(255,255,255,1)'
      ],
      spanGaps: true,
      borderWidth: 1.7,
      radius: 0
    }],
    fontColor: "black"
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans",
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }],
    },
    layout: {
      padding: {
        left: 10,
        right: 15,
        top: 20,
        bottom: 10
      }
    },
    legend: {
      display: false
    }
  }
});