var usda = document.getElementById("usda-chart");
var usdaChart = new Chart(usda, {
  type: 'line',
  data: {
    labels: ["5", "6", "7", "8", "9", "10"],
    datasets: [{
      label: 'USDA price',
      data: [12, 19, 3, 5, 2, 3],
      borderColor: [
        'rgba(255,255,255,1)'
      ],
      spanGaps: true,
      borderWidth: 1.7,
      radius: 0
    }],
    fontColor: "black"
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        },
        scaleLabel: {
          display: true,
          labelString: "Price",
          fontColor: "#fff",
          padding: 0,
          fontFamily: '"IBM Plex Sans", "Arial", sans-serif'
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans",
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }],
    },
    layout: {
      padding: {
        left: 10,
        right: 15,
        top: 20,
        bottom: 10
      }
    },
    legend: {
      display: false
    }
  }
});

var btca = document.getElementById("btca-chart");
var btcaChart = new Chart(btca, {
  type: 'line',
  data: {
    labels: ["5", "6", "7", "8", "9", "10"],
    datasets: [{
      label: 'BTCA price',
      data: [1, 6, 8, 16, 5, 9],
      borderColor: [
        'rgba(255,255,255,1)'
      ],
      spanGaps: true,
      borderWidth: 1.7,
      radius: 0
    }],
    fontColor: "black"
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        },
        scaleLabel: {
          display: true,
          labelString: "Price",
          fontColor: "#fff",
          padding: 0,
          fontFamily: '"IBM Plex Sans", "Arial", sans-serif',
          position: 'top'
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans",
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }],
    },
    layout: {
      padding: {
        left: 10,
        right: 15,
        top: 20,
        bottom: 10
      }
    },
    legend: {
      display: false
    }
  }
});

var etha = document.getElementById("etha-chart");
var ethaChart = new Chart(etha, {
  type: 'line',
  data: {
    labels: ["5", "6", "7", "8", "9", "10"],
    datasets: [{
      label: 'ETHA price',
      data: [5, 7, 1, 5, 9, 15],
      borderColor: [
        'rgba(255,255,255,1)'
      ],
      spanGaps: true,
      borderWidth: 1.7,
      radius: 0
    }],
    fontColor: "black"
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        },
        scaleLabel: {
          display: true,
          labelString: "Price",
          fontColor: "#fff",
          padding: 0,
          fontFamily: '"IBM Plex Sans", "Arial", sans-serif',
          position: 'top'
        }
      }],
      xAxes: [{
        ticks: {
          fontColor: "white",
          fontSize: 10,
          fontFamily: "IBM Plex Sans",
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }],
    },
    layout: {
      padding: {
        left: 10,
        right: 15,
        top: 20,
        bottom: 10
      }
    },
    legend: {
      display: false
    }
  }
});